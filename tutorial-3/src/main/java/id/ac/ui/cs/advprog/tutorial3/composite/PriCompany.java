package id.ac.ui.cs.advprog.tutorial3.composite;

import id.ac.ui.cs.advprog.tutorial3.composite.higherups.Ceo;
import id.ac.ui.cs.advprog.tutorial3.composite.higherups.Cto;
import id.ac.ui.cs.advprog.tutorial3.composite.techexpert.NetworkExpert;
import id.ac.ui.cs.advprog.tutorial3.composite.techexpert.SecurityExpert;
import id.ac.ui.cs.advprog.tutorial3.composite.techexpert.UiUxDesigner;

public class PriCompany {
    public static void main(String[] args) {
        Company company = new Company();

        try {
            Employees cinderella = new Ceo("Cinderella", 350000);
            company.addEmployee(cinderella);
            Employees belle = new Cto("Belle", 335000.00);
            company.addEmployee(belle);

            Employees rapunzel = new UiUxDesigner("Rapunzel", 188000.00);
            company.addEmployee(rapunzel);

            Employees mulan = new NetworkExpert("Mulan", 87000.00);
            company.addEmployee(mulan);

            Employees vanellope = new SecurityExpert("Vanellope", 99.00);
            company.addEmployee(vanellope);
        } catch (IllegalArgumentException e) {
            System.out.println(e.getMessage());
        } finally {
            System.out.println(company.getNetSalaries());
        }


    }
}
