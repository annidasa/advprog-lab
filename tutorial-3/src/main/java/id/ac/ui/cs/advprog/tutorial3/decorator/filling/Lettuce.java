package id.ac.ui.cs.advprog.tutorial3.decorator.filling;

import id.ac.ui.cs.advprog.tutorial3.decorator.Food;

public class Lettuce extends Filling {

    public Lettuce(Food food) {
        this.food = food;
        description = "lettuce";
    }

    @Override
    public String getDescription() {
        return food.getDescription() + ", adding " + description;
    }

    @Override
    public double cost() {
        return food.cost() + 0.75;
    }
}
