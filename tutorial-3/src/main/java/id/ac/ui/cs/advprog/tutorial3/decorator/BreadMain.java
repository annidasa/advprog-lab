package id.ac.ui.cs.advprog.tutorial3.decorator;

import id.ac.ui.cs.advprog.tutorial3.decorator.bread.CrustySandwich;
import id.ac.ui.cs.advprog.tutorial3.decorator.filling.BeefMeat;
import id.ac.ui.cs.advprog.tutorial3.decorator.filling.Cheese;
import id.ac.ui.cs.advprog.tutorial3.decorator.filling.TomatoSauce;

public class BreadMain {
    public static void main(String[] args) {
        Food cheeseSandwich = new Cheese(new CrustySandwich());
        System.out.println(cheeseSandwich.getDescription());
        System.out.println(cheeseSandwich.cost());

        Food beefCheeseSandwich = new BeefMeat(cheeseSandwich);
        System.out.println(beefCheeseSandwich.getDescription());
        System.out.println(beefCheeseSandwich.cost());

        Food tomatoSauce = new TomatoSauce(beefCheeseSandwich);
        System.out.println(tomatoSauce.getDescription());
        System.out.println(tomatoSauce.cost());

    }
}
