package id.ac.ui.cs.advprog.tutorial3.decorator.filling;

import id.ac.ui.cs.advprog.tutorial3.decorator.Food;

public class BeefMeat extends Filling {

    public BeefMeat(Food food) {
        this.food = food;
        description = "beef meat";
    }

    @Override
    public String getDescription() {
        return food.getDescription() + ", adding " + description;
    }

    @Override
    public double cost() {
        return food.cost() + 6.00;
    }
}
