package id.ac.ui.cs.advprog.tutorial4.exercise1.factory;

import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.cheese.Cheese;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.cheese.MozarellaCheese;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.clam.CheeseCrustDough;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.clam.Clams;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.dough.Dough;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.sauce.MayoSauce;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.sauce.Sauce;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.veggies.*;

//TODO Make sure your implementation of DepokPizzaIngredientFactory does not use the same ingredients provided by NewYorkPizzaIngredientFactory
public class DepokPizzaIngredientFactory implements PizzaIngredientFactory {

    @Override
    public Dough createDough() {
        return new id.ac.ui.cs.advprog.tutorial4.exercise1.factory.dough.CheeseCrustDough();
    }

    @Override
    public Sauce createSauce() {
        return new MayoSauce();
    }

    @Override
    public Cheese createCheese() {
        return new MozarellaCheese();
    }

    @Override
    public Veggies[] createVeggies() {
        Veggies[] veggies = {new BlackOlives(), new Eggplant(), new Nori(), new Spinach()};
        return veggies;
    }

    @Override
    public Clams createClam() {
        return new CheeseCrustDough();
    }
}
