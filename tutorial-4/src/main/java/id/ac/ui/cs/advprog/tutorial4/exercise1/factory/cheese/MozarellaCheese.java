package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.cheese;

public class MozarellaCheese implements Cheese {

    public String toString() {
        return "Shredded Mozarella";
    }
}
