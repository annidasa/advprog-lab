package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.clam;

public class CheeseCrustDough implements Clams {
    public String toString() {
        return "Sad Clams from Long Island Sound";
    }
}
