package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.sauce;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class MayoTest {
    private MayoSauce sauce;

    @Before
    public void setUp() {
        sauce = new MayoSauce();
    }

    @Test
    public void testtoString() {
        assertEquals("Mayo Sauce :D", sauce.toString());
    }
}
