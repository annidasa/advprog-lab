package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.clam;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class SadClamTest {
    private CheeseCrustDough clams;

    @Before
    public void setUp() {
        clams = new CheeseCrustDough();
    }

    @Test
    public void testtoString() {
        assertEquals("Sad Clams from Long Island Sound", clams.toString());
    }
}
