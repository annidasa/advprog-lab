package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.cheese;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class ReggianoCheeseTest {
    private ReggianoCheese cheese;

    @Before
    public void setUp() {
        cheese = new ReggianoCheese();
    }

    @Test
    public void testMethodCost() {
        assertEquals("Reggiano Cheese", cheese.toString());
    }
}
