package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.cheese;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class MozarellaCheeseTest {
    private MozarellaCheese cheese;

    @Before
    public void setUp() {
        cheese = new MozarellaCheese();
    }

    @Test
    public void testToString() {
        assertEquals("Shredded Mozarella", cheese.toString());
    }
}
