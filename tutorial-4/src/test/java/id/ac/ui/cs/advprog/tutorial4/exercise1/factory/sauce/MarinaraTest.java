package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.sauce;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class MarinaraTest {
    private MarinaraSauce sauce;

    @Before
    public void setUp() {
        sauce = new MarinaraSauce();
    }

    @Test
    public void testtoString() {
        assertEquals("Marinara Sauce", sauce.toString());
    }
}
