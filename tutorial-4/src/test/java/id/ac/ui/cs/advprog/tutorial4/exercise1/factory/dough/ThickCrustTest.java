package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.dough;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class ThickCrustTest {
    private ThickCrustDough dough;

    @Before
    public void setUp() {
        dough = new ThickCrustDough();
    }

    @Test
    public void testtoString() {
        assertEquals("ThickCrust style extra thick crust dough", dough.toString());
    }
}
