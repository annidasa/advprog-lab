package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.cheese;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class CheddarCheeseTest {
    private CheddarCheese cheddarCheese;

    @Before
    public void setUp() {
        cheddarCheese = new CheddarCheese();
    }

    @Test
    public void testMethodCost() {
        assertEquals("Shredded Cheddar", cheddarCheese.toString());
    }

}