package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.dough;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class CheeseCrustTest {
    private CheeseCrustDough dough;

    @Before
    public void setUp() {
        dough = new CheeseCrustDough();
    }

    @Test
    public void testtoString() {
        assertEquals("Cheese crust best crust", dough.toString());
    }
}
