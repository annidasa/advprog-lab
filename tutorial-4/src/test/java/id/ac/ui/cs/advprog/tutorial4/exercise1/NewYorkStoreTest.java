package id.ac.ui.cs.advprog.tutorial4.exercise1;

import org.junit.Before;
import org.junit.Test;

import static org.junit.jupiter.api.Assertions.assertNotNull;

public class NewYorkStoreTest {
    private NewYorkPizzaStore store;

    @Before
    public void setUp() {
        store = new NewYorkPizzaStore();
    }

    @Test
    public void testCreatePizza() {
        assertNotNull(store.createPizza("cheese"));
        assertNotNull(store.createPizza("veggie"));
        assertNotNull(store.createPizza("clam"));
    }
}
