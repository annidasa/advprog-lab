package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.veggies;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class NoriTest {
    private Nori veggies;

    @Before
    public void setUp() {
        veggies = new Nori();
    }

    @Test
    public void testtoString() {
        assertEquals("Nori", veggies.toString());
    }
}
