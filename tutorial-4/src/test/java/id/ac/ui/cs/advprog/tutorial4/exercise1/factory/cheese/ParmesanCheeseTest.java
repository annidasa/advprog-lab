package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.cheese;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class ParmesanCheeseTest {
    private ParmesanCheese cheese;

    @Before
    public void setUp() {
        cheese = new ParmesanCheese();
    }

    @Test
    public void testMethodCost() {
        assertEquals("Shredded Parmesan", cheese.toString());
    }
}
