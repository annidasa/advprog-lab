package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.sauce;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class PlumTomatoTest {
    private PlumTomatoSauce sauce;

    @Before
    public void setUp() {
        sauce = new PlumTomatoSauce();
    }

    @Test
    public void testtoString() {
        assertEquals("Tomato sauce with plum tomatoes", sauce.toString());
    }
}
