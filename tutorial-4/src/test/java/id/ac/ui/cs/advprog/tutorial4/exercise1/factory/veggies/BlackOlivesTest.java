package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.veggies;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class BlackOlivesTest {
    private BlackOlives sauce;

    @Before
    public void setUp() {
        sauce = new BlackOlives();
    }

    @Test
    public void testtoString() {
        assertEquals("Black Olives", sauce.toString());
    }

}
